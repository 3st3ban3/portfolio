// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  app: {
    head: {
      titleTemplate: '%s - Esteban',
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1'
    },
    baseURL: '/portfolio/'
  },
  modules: ["@nuxt/content"],
  css: ["@/assets/css/main.css"],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
});
