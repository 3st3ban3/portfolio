# Portfolio

You can also read my <a href="https://3st3ban3.gitlab.io/resume/" target="_blank">full resume</a>

## BuildrMetrics

Web application to discover and simulate high-performing liquidity pools on decentralized exchanges to optimize decentralized finance investments.

![Discover page](/projects/buildrmetrics/1.png)
![Simulate page](/projects/buildrmetrics/2.png)

## Trophereum

Decentralized web application that brings transparency, security, and efficiency to the organization of events, awards, and giveaways through blockchain.

![Home page](/projects/trophereum/1.png)
![Event page](/projects/trophereum/2.png)

## Mon Suivi Social

Web application realized while working with the French "Incubateur des Territoires" (beta.gouv.fr), used by municipalities for following up on social aids.

![Statistics page](/projects/mss/1.png)
![Home page](/projects/mss/2.png)

## Topnetworky

Full stack development of a web application that connects candidates and recruiters.
Users can create an account as a recruiter in order to receive notifications for new candidates, or as a candidate to share their profile and skills.

![Home page](/projects/topnetworky/1.png)
![Recruiters page](/projects/topnetworky/2.png)

## uMap

Improvements to uMap, a platform to easily create interactive maps, while working on the CoCO2 project, a mission to monitor the human impact on CO2, ordered by ICOS, a European-wide greenhouse gas research infrastructure.

![Filters feature](/projects/uMap/1.png)

## Rivage Tournament

Web application to organize video game tournaments online, developed while working for Rivage Esport.

![Tournaments page](/projects/rivage/1.png)
![Home page](/projects/rivage/2.png)
![Account page](/projects/rivage/3.png)